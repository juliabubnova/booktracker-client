import {
  AnyAction,
  applyMiddleware,
  combineReducers,
  createStore,
} from 'redux';
import thunk, { ThunkDispatch } from 'redux-thunk';
import { classActionMiddleware } from './classActionMiddleware';
import { bookReducer } from 'Books/state/bookReducer';
import { initialState } from './initialState';
import { authReducer } from 'Auth/state/authReducer';
import { IState } from 'Core/State/IState';

const rootReducer = combineReducers({
  books: bookReducer,
  auth: authReducer,
});

export const store = createStore(
  rootReducer,
  initialState,
  applyMiddleware(classActionMiddleware, thunk),
);

export type TDispatch = ThunkDispatch<IState, {}, AnyAction>;
