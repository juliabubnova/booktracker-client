import { TDispatch } from 'Core/State/store';
import axios from 'axios';
import { API } from 'environment';
import { LoginAction } from 'Auth/state/actions/AddBookAction';
import { ICredentials } from 'Auth/state/interfaces/ICredentials';

// function dummyLogin(): Promise<User> {
//   return new Promise((resolve) =>
//     setTimeout(
//       () =>
//         resolve({
//           email: 'my@email.com',
//           firstName: 'Julia',
//           lastName: 'Bubnova',
//         } as User),
//       2000,
//     ),
//   );
// }

export const signIn = ({ email, password }: ICredentials) => async (
  dispatch: TDispatch,
) => {
  const response = await axios.post(`${API}login`, { name: email, password });
  const token = response.data as string;
  console.log(token);
  dispatch(new LoginAction(email, token));
};
