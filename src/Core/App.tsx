import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { MainContainer } from 'Core/components/Main';

export function App() {
  return <MainContainer />;
}
