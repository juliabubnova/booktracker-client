import { Action } from 'redux';

export class LoginAction implements Action {
  public static readonly type = 'auth/login';
  public readonly type = LoginAction.type;

  public constructor(readonly email: string, readonly token: string) {}
}
