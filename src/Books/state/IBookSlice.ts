import { IBook } from 'Books/List/interfaces/IBook';

export interface IBookSlice {
  books: IBook[];
  loading: boolean;
}
