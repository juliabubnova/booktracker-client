import { IState } from 'Core/State/IState';

export const selectBooks = (state: IState) => state.books.books;
