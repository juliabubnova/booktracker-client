import { createSelector } from 'reselect';
import { selectAuth } from 'Auth/state/selectors/selectAuth';
import { IAuthSlice } from 'Auth/state/IAuthSlice';

export const selectToken = createSelector(
  [selectAuth],
  (auth: IAuthSlice) => auth.token,
);
