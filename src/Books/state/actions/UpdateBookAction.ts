import { IBook } from 'Books/List/interfaces/IBook';
import { Action } from 'redux';

export class UpdateBookAction implements Action {
  public static readonly type = 'book/update_book';
  public readonly type = UpdateBookAction.type;

  public constructor(readonly book: IBook) {}
}
