import { IBookSlice } from '../../Books/state/IBookSlice';
import { IAuthSlice } from '../../Auth/state/IAuthSlice';

export interface IState {
  books: IBookSlice;
  auth: IAuthSlice;
}
