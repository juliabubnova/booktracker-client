import { connect } from 'react-redux';
import { LoginForm } from 'Auth/Login/LoginForm';
import { LoginMode } from 'Auth/Login/enums/LoginMode';
import { signUp } from 'Auth/state/thunks/signUp';
import { IState } from 'Core/State/IState';
import { selectLoggedIn } from 'Auth/state/selectors/selectLoggedIn';

const mapDispatchToProps = {
  onSubmit: signUp,
};

const mapStateToProps = (state: IState) => ({
  mode: LoginMode.SIGN_UP,
  loggedIn: selectLoggedIn(state),
});

export const SignUpContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(LoginForm);
