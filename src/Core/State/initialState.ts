import { IState } from './IState';

export const initialState: IState = {
  books: {
    books: [],
    loading: true,
  },
  auth: {
    token: undefined,
    email: undefined,
    // firstName: 'John',
    // lastName: 'Doe',
  },
};
