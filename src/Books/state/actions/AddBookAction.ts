import { IBook } from 'Books/List/interfaces/IBook';
import { Action } from 'redux';

export class AddBookAction implements Action {
  public static readonly type = 'book/add_book';
  public readonly type = AddBookAction.type;

  public constructor(readonly book: IBook) {}
}
