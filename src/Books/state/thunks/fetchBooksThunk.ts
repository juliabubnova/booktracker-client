import { API } from 'environment';
import { IBook } from 'Books/List/interfaces/IBook';
import axios from 'axios';
import { TDispatch } from 'Core/State/store';
import { SetBooksAction } from 'Books/state/actions/SetBooksAction';
import { IState } from 'Core/State/IState';
import { selectToken } from 'Auth/state/selectors/selectToken';

export const fetchBooksThunk = () => async (
  dispatch: TDispatch,
  getState: () => IState,
) => {
  const state = getState();
  const token = selectToken(state);
  const response = await axios.get(`${API}books`, {
    headers: {
      Authentication: `Bearer ${token}`,
    },
  });
  const books = response.data as IBook[];
  dispatch(new SetBooksAction(books));
};
