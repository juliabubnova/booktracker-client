import { TDispatch } from 'Core/State/store';
import { IState } from 'Core/State/IState';
import { selectToken } from 'Auth/state/selectors/selectToken';
import { IBook } from 'Books/List/interfaces/IBook';
import { updateBook } from 'Books/state/thunks/updateBook';
import { UpdateBookAction } from 'Books/state/actions/UpdateBookAction';

export const updateBookThunk = (book: IBook) => async (
  dispatch: TDispatch,
  getState: () => IState,
) => {
  const state = getState();
  const token = selectToken(state);
  await updateBook(book, token);
  // if (response.status !== 200){
  //   dispatch error
  // }
  dispatch(new UpdateBookAction(book));
};
