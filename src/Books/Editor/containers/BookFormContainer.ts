import { connect } from 'react-redux';
import { TDispatch } from 'Core/State/store';
import { BookForm } from 'Books/Editor/components/BookForm';
import { IBook } from 'Books/List/interfaces/IBook';
import { updateBookThunk } from 'Books/state/thunks/updateBookThunk';

const mapDispatchToProps = (
  dispatch: TDispatch,
  ownProps: { setEditFinished?: () => void },
) => ({
  onSubmit: (book: IBook) => {
    // todo extract thunk
    dispatch(updateBookThunk(book)).then(() => {
      if (ownProps?.setEditFinished !== undefined) {
        ownProps.setEditFinished();
      }
    });
  },
}); // todo handle errors

export const BookFormContainer = connect(null, mapDispatchToProps)(BookForm);
