import { AddBookAction } from 'Books/state/actions/AddBookAction';
import produce from 'immer';
import { IBookSlice } from 'Books/state/IBookSlice';
import { UpdateBookAction } from 'Books/state/actions/UpdateBookAction';
import { SetBooksAction } from 'Books/state/actions/SetBooksAction';
import { initialState } from 'Core/State/initialState';

type TAction = AddBookAction | UpdateBookAction | SetBooksAction;

export const bookReducer = produce(
  (state: IBookSlice = initialState.books, action: TAction) => {
    switch (action.type) {
      case AddBookAction.type: {
        state.books.push(action.book);
        return;
      }

      case SetBooksAction.type: {
        state.books = action.books;
        state.loading = false;
        return;
      }

      case UpdateBookAction.type: {
        const currentBook = state.books.find(
          (book) => book.id === action.book.id,
        );
        if (currentBook) {
          currentBook.description = action.book.description;
          currentBook.name = action.book.name;
          currentBook.progress = action.book.progress;
        }
        return;
      }

      default:
        return state;
    }
  },
);
