import { connect } from 'react-redux';
import { selectBooks } from 'Books/state/selectors/selectBooks';
import { fetchBooksThunk } from 'Books/state/thunks/fetchBooksThunk';
import { BookList } from 'Books/List/components/BookList';
import { IState } from 'Core/State/IState';

const mapStateToProps = (state: IState) => ({
  books: selectBooks(state),
});

const mapDispatchToProps = {
  getAllBooks: fetchBooksThunk,
};

export const BookListContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(BookList);
