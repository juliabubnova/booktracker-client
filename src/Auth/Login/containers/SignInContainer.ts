import { connect } from 'react-redux';
import { LoginForm } from 'Auth/Login/LoginForm';
import { LoginMode } from 'Auth/Login/enums/LoginMode';
import { signIn } from 'Auth/state/thunks/signIn';
import { selectLoggedIn } from 'Auth/state/selectors/selectLoggedIn';
import { IState } from 'Core/State/IState';

const mapStateToProps = (state: IState) => ({
  mode: LoginMode.SIGN_IN,
  loggedIn: selectLoggedIn(state),
});

const mapDispatchToProps = {
  onSubmit: signIn,
};

export const SignInContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(LoginForm);
