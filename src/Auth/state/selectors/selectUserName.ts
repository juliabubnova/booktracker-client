import { selectAuth } from 'Auth/state/selectors/selectAuth';
import { createSelector } from 'reselect';
import { IAuthSlice } from 'Auth/state/IAuthSlice';

export const selectUserName = createSelector(
  [selectAuth],
  (auth: IAuthSlice) => auth.name || auth.email || 'Anon',
);
