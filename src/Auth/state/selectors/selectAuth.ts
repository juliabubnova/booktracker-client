import { IState } from 'Core/State/IState';

export const selectAuth = (state: IState) => state.auth;
