import { TDispatch } from 'Core/State/store';
import axios from 'axios';
import { API } from 'environment';
import { LoginAction } from 'Auth/state/actions/AddBookAction';
import { ICredentials } from 'Auth/state/interfaces/ICredentials';

export const signUp = ({ email, password }: ICredentials) => async (
  dispatch: TDispatch,
) => {
  const response = await axios.post(`${API}register`, {
    name: email,
    password,
  });
  const token = response.data as string;
  dispatch(new LoginAction(email, token));
};
