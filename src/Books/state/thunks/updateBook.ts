import axios from 'axios';
import { API } from 'environment';
import { IBook } from 'Books/List/interfaces/IBook';

export function updateBook(book: IBook, token?: string) {
  return axios.post(`${API}update_book`, book, {
    headers: {
      Authentication: `Bearer ${token}`,
    },
  });
}
