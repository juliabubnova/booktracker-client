import { IBook } from 'Books/List/interfaces/IBook';
import { Action } from 'redux';

export class SetBooksAction implements Action {
  public static readonly type = 'book/set_books';
  public readonly type = SetBooksAction.type;

  public constructor(readonly books: IBook[]) {}
}
