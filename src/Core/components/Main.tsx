import React from 'react';
import { BrowserRouter as Router, Link, Route } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import Logo from '../../assets/logo.png';
import { IState } from 'Core/State/IState';
import { connect } from 'react-redux';
import { BookListContainer } from 'Books/List/containers/BookListContainer';
import { EditPage } from 'Books/Editor/components/EditPage';
import { CreateBookPage } from 'Books/Editor/components/CreateBookPage';
import { LoginPage } from 'Auth/Login/LoginPage';
import { selectLoggedIn } from 'Auth/state/selectors/selectLoggedIn';
import { selectUserName } from 'Auth/state/selectors/selectUserName';

interface IProps {
  name?: string;
  loggedIn: boolean;
}

export function Main({ name, loggedIn }: IProps) {
  return (
    <Router>
      <div className="container">
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
          <a className="navbar-brand" target="_blank" href="/">
            <img src={Logo} width="30" height="30" alt="Logo" />
          </a>
          <Link to="/" className="navbar-brand">
            {' '}
            Book Tracker
          </Link>
          <div className="collapse navbar-collapse">
            <ul className="navbar-nav mr-auto">
              <li className="navbar-item">
                <Link to="/" className="nav-link">
                  Books
                </Link>
              </li>
              <li className="navbar-item">
                <Link to="/create" className="nav-link">
                  Create Book
                </Link>
              </li>
            </ul>
            <ul className="navbar-nav ml-auto">
              <li className="navbar-item">
                {!loggedIn && (
                  <Link to="/login" className="nav-link">
                    Login
                  </Link>
                )}

                {loggedIn && name}
              </li>
            </ul>
          </div>
        </nav>
        <br />

        <Route path="/" exact component={BookListContainer} />
        <Route path="/edit/:id" component={EditPage} />
        <Route path="/create" component={CreateBookPage} />
        <LoginPage />
      </div>
    </Router>
  );
}

const mapStateToProps = (state: IState) => ({
  loggedIn: selectLoggedIn(state),
  name: selectUserName(state),
});

export const MainContainer = connect(mapStateToProps)(Main);
