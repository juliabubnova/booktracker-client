import { createSelector } from 'reselect';
import { selectAuth } from 'Auth/state/selectors/selectAuth';
import { IAuthSlice } from 'Auth/state/IAuthSlice';

export const selectLoggedIn = createSelector([selectAuth], (auth: IAuthSlice) =>
  Boolean(auth.token),
);
