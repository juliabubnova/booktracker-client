import produce from 'immer';

import { initialState } from 'Core/State/initialState';
import { LoginAction } from 'Auth/state/actions/AddBookAction';
import { IAuthSlice } from 'Auth/state/IAuthSlice';

// export const authReducer = createSlice({
//   name: 'auth',
//   initialState: {} as AuthState,
//   reducers: {
//     logout(draft, action) {
//       draft.user = undefined;
//     },
//   },

type TAction = LoginAction;

export const authReducer = produce(
  (state: IAuthSlice = initialState.auth, action: TAction) => {
    switch (action.type) {
      case LoginAction.type: {
        state.token = action.token;
        state.email = action.email;
        return;
      }

      default:
        return state;
    }
  },
);
